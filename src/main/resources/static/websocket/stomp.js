$(document).ready(function() {

	let loggedUser = {
		username : '',
		authorities : [],
		accountNonExpired : false,
		accountNonLocked : false,
		credentialsNonExpired : false,
		enabled : false
	};
	
	function getPrincipal() {
		$.ajax({
			url: '/users/principal',
			method: 'GET',
			success: function(data, status) {
				loggedUser = data;
				console.log(loggedUser);
			}
		});
	}
	
	let stompClient = null;
	
	function connect() {
		//alert('connecting');
		let message = "";
		
		if(window.WebSocket) {
			message = "supported";
			console.log("BROWSER SUPPORTED");
		} else {
			console.log("BROWSER NOT SUPPORTED");
		}
		
		let socket = new SockJS("http://localhost:8080/inbox");
		// initializing connection
		stompClient = Stomp.over(socket);
		
		stompClient.connect({}, function(frame) {
			//alert('in client');
			stompClient.subscribe("/my-user-des/my-simple-broker/reply", function(response) {
				//alert("subscribing");
				console.log("> from client");
				console.log(response);
				showResult(JSON.parse(response.body));
			});
		});
	}
	
	function sendMessage() {
		
		// getting the entered message
		let inboxText = $("#input-inbox").val();
		
		// getting send to user
		let sendToUser = $("#input-inbox").attr("data-to-username");
		let toUserNames = [loggedUser.username, sendToUser];
		stompClient.send("/chat-app/inbox", {}, JSON.stringify({ 'fromUserName': loggedUser.username, 'text': inboxText, 'toUserNames': toUserNames }));
		
		$("#input-inbox").val(null);
	}
	
	function showResult(message) {
		let dateStr = moment().format("ddd DD, MM/YYYY, HH:mm A");
		let isLoggedUser = message.fromUserName == loggedUser.username ? true : false;
		
		if(isLoggedUser) {
			$("#inbox").append(`
				<div class="col-xs-10 text-right">
					<h3>#me <small>${dateStr}</small></h3>
					<p>${message.text}</p>
					<br/>
				</div>
				<div class="col-sm-2 text-center">
					<img class="img-circle" src="https://forums.roku.com/styles/canvas/theme/images/no_avatar.jpg" width="65" height="65" />
				</div>
				<div class="clearfix"></div>
			`);
		} else {
			$("#inbox").append(`
				<div class="col-sm-2 text-center">
					<img class="img-circle" src="https://forums.roku.com/styles/canvas/theme/images/no_avatar.jpg" width="65" height="65" />
				</div>
				<div class="col-xs-10 text-left">
					<h3>${message.fromUserName} <small>${dateStr}</small></h3>
					<p>${message.text}</p>
					<br/>
				</div>
				<div class="clearfix"></div>
			`);
		}
	}
	
	$('#btn-send-inbox').click(function() {
		sendMessage();
	});
	
	
	$( "#input-inbox" ).keydown(function(e) {
		if(e.which == 13) {
			console.log( `Handler for enter event called.` );
			sendMessage();
		}
	});
	
	(function IIFE() {
		// initializing connection.
		connect();
		// getting principal
		getPrincipal();
	})();
})