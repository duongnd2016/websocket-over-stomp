package com.ocean.stomp.categories.dao.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Message {
	
	private String fromUserName;
	private String[] toUserNames;
	private String text;
}