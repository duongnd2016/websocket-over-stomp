package com.ocean.stomp.categories;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSocketOverStompApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebSocketOverStompApplication.class, args);
	}
}
