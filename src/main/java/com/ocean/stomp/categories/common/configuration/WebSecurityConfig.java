package com.ocean.stomp.categories.common.configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Bean
	public InMemoryUserDetailsManager inMemoryUserDetailsManager() {
		
		ClassLoader classLoader = getClass().getClassLoader();
		String myusersPath = classLoader.getResource("").getPath() + "/myusers.properties";
		
		final Properties users = new Properties();
		
		try(InputStream input = new FileInputStream(myusersPath)) {
			
			users.load(input);
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return new InMemoryUserDetailsManager(users);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.userDetailsService(inMemoryUserDetailsManager());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http	
			.authorizeRequests()
				.anyRequest()
				.authenticated()
			.and()
				.formLogin()
					.loginProcessingUrl("/perform-login")
					.defaultSuccessUrl("/web-socket/home", true)
			.and()
				.logout()
					.logoutSuccessUrl("/login?logout")
			.and()
				.cors()
			.and()
				.csrf()
					.disable();
				
	}

	
}
