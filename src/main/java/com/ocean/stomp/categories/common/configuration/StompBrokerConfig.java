package com.ocean.stomp.categories.common.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
public class StompBrokerConfig extends AbstractWebSocketMessageBrokerConfigurer {

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		// Registering a STOMP over WebSocket endpoint at the given mapping path.
		registry.addEndpoint("/inbox").setAllowedOrigins("*").withSockJS();
	}

	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
		
		config.enableSimpleBroker("/topic", "/queue", "/my-simple-broker");
		config.setApplicationDestinationPrefixes("/chat-app");
		// the default user destination is '/user'
		config.setUserDestinationPrefix("/my-user-des");
	}
	
}
