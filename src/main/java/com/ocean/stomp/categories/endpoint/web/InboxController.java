package com.ocean.stomp.categories.endpoint.web;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ocean.stomp.categories.dao.model.Message;
import com.ocean.stomp.categories.services.UserService;

@Controller
@RequestMapping("/web-socket")
public class InboxController {

	private UserService userService;
	
	private SimpMessagingTemplate simpMessagingTemplate;

	public InboxController(UserService userService, SimpMessagingTemplate simpMessagingTemplate) {
		super();
		this.userService = userService;
		this.simpMessagingTemplate = simpMessagingTemplate;
	}

	@ModelAttribute("user")
	private User getPrincipal() {

		return userService.getPrincipal();
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String homePage() {
		
		return "home";
	}
	
	@RequestMapping(value = "/send-to/{toUserName}")
	public String sendToUser(Model model, @PathVariable String toUserName) {
		
		model.addAttribute("toUserName", toUserName);
		
		return "inbox";
	}
	
	@MessageMapping("/inbox")
	public void retrieveMessage(Message message) {
		
		for(String toUserName: message.getToUserNames()) {
			
			simpMessagingTemplate.convertAndSendToUser(toUserName, "/my-simple-broker/reply", message);
		}
	}
	
}
