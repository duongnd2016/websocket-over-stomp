package com.ocean.stomp.categories.endpoint.resources;

import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocean.stomp.categories.services.UserService;

@RestController
@RequestMapping("/users")
public class UserResource {

	private UserService userService;

	public UserResource(UserService userService) {
		super();
		this.userService = userService;
	}
	
	@GetMapping("/principal")
	public User getPrincipal() {
		
		return userService.getPrincipal();
	}
}
