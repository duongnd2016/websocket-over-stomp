package com.ocean.stomp.categories.services;

import org.springframework.security.core.userdetails.User;

public interface UserService {
	
	User getPrincipal();
}
