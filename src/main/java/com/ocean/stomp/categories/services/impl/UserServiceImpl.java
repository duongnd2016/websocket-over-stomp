package com.ocean.stomp.categories.services.impl;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import com.ocean.stomp.categories.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Override
	public User getPrincipal() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Object obj = auth.getPrincipal();
		if (obj instanceof User) {
			return (User) obj;
		}
		return null;
	}

}
